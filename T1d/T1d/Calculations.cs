﻿using System;


namespace MajorCalculations
{
    /*------
     
     * ------ ************class calculations************ 
     * 
     * This class contains all the calculations that are required for this diabetes management system , which includes constants , 
     * mathematical equations.
     * 
     * Calculations - is the only class that i have used in this namespace - MajorCalculations
        *Major functions inside the class
            *isf()
            *calcarb()
            *cir()
            *carbCount()
            *totalDoseCalculation()
            *correctionFactorFunc()
            *convertSugarLevel()
            
     -------*/



    public class calculations
    {

        /*------ Function isf()    to calculate blood sensitivity
                    
                     * insulinSensityvityFactor      - Insulin sensitivity factor(ISF)-it is to identifify blood sensitivity towards insulin.
                     * totalDailyDose                - the varable contains the total insulin dose taken a day
                     * k                             -  k= 1800 is the standard constant used to calculate blood sensitivity
                     * insulinSensitivityFactor      - variable used to store insulin sensitivity factor
        -------*/
        public int isf(int value)
        {

            int insulinSensitivityFactor = 0;
            try
            {
                int toalDailyDose = value;
                int k = 1800;
                insulinSensitivityFactor = k / toalDailyDose;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return insulinSensitivityFactor;
        }

        //------end of isf() ------



        /* ---------------      Function    calCarb() to calculate carb for 1 gram of a particular item
               
                 *  carbContent     - its the total amount of carb per 100 gram of the food item
                 *  quantityCarbF   - its the total amount of food plan to intake,in grams
                 *  totCarbF        - the variable used to store total carb value
         */
        public double calCarb(double carbContent, double quantityCarbF)
        {
            double totCarbF = 0;
            try
            {
                double carbF = carbContent;
                double qCarbF = quantityCarbF;

                 totCarbF = (carbF / 100) * qCarbF;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           

            return totCarbF;
        }
        //----------end of ----calCarb()----------


        /*------------ Function  cir() to calulate carb to insulin ratio
         
            * carbToInsulinRatio    - Carb To Insulin Ratio(CIR) - it gives the detail about the insuin required for 1 gram of carb
            * totalDailyDose, tdd   - the varable contains the total insulin dose taken a day
            * weight, cWeight       - current weight of the human body in kilo grams
            * k                     - standard constant used for CIR calculation
            * carbToInsulinRatio    - variable used for storing carb to insulin ratio(CIR)
    
         */

        public double cir(int tdd, double weight)
        {
            
            double carbToInsulinRatio = 0;
           
            try
            {
                int totalDailyDose = tdd;
                double cWeight = weight;
                double k = 5.7;
                carbToInsulinRatio = ((k * cWeight) / totalDailyDose);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            return carbToInsulinRatio;
        }
        //---------end of cir()-----



        /*-------- Function carbCount(),is used to count carb
         * 
         *carbToInsulinRatio        - variables used for storing carb to insulin ratio
         *carbCountMeasured         - variable used for storing total carb count measured
         *cir                       - parameter passed to the method to store carb to insulin ratio
         *ccm                       - parameter passed to the method to store  total carb count measured
         *result                    - variable used to store the end result, the total insulin required for the body
         
         ---------*/
        public double carbCount(double cir, double ccm)
        {
            
            double result = 0;
            try
            {
                double carbToInsulinRatio = cir;
                double carbCountMeasured = ccm;
                result = carbCountMeasured / carbToInsulinRatio;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }

        //-------end of carbCount()-----




        /*---Function  totalDoseCalculation()- calucalte total insulin taken in a day
         * 
         * dailyTotalDose               - variable used to store the total daily dose of insulin
         * numOfTimeInsulinTaken        - variable used to store total number of time insulin taken a day
         * 

        ----*/
        public int totalDoseCalculation()
        {
            int dailyTotalDose = 0;
            int numOfTimeInsulinTaken;
            do
            {
                try
                {
                    Console.Write("\n\tEnter the number of times you take insulin   :   ");
                     numOfTimeInsulinTaken = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (FormatException e)
                {
                    Console.WriteLine("\n\n\tinvalid input");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            } while (true);

            
            try
            {
                for (int i = 0; i < numOfTimeInsulinTaken; i++)
                {
                    do
                    {
                        try
                        {
                            Console.Write("\n\tEnter the dosage for the  " + (i + 1) + "   intake :     ");
                            int x = Convert.ToInt32(Console.ReadLine());
                            dailyTotalDose += x;
                            break;
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("\n\n\tinvalid input");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                    } while (true);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }

            return dailyTotalDose;
        }

        //--------end of totalDoseCalculation()--------



        /*------ Function correctionFactorFunc() -to calculate correction factor
                 * csl                      - parameter used for storing current sugar level
                 * rgl                      - parameter used for storing required sugar level
                 * isf                      - parameter used for storing insulin sensitivity factor
                 * result                   - the varaiblein which final correction factor result stored
         --------*/
        public double correctionFactorFunc(double csl, double rgl, int isf)
        {
            double result = 0;
            try
            {
                double currentSugarlevel = csl;
                double requiredSugarLevel = rgl;
                int insulinSensitivityFactor = isf;
                result = (currentSugarlevel - requiredSugarLevel) / insulinSensitivityFactor;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return result;
        }
        //------end of correctionFactorFunc()------




        /*-------- function convertSugarLevel() -to convert sugar from mmol/l to mg/dl
         * 
         * reading              - parameter passed to store glucometer sugar level reading
         * conversionConst      - is the standard constant for conversion of mmol/l to mg/dl
         * result               - variable used to store the end result

         ---------*/
        public double convertSugarLevel(double reading)
        {
            double result = 0;
            try
            {
                double value = reading;
                int conversionConst = 18;
                result = value * conversionConst;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            
            return result;

        }

        //---------end of convertSugarLevel()------


    }

    //-----------------------*************end of class calculations************ ----------------------------------//

}
