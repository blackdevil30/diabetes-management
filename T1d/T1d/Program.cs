﻿using System;
using System.Threading.Tasks;
using UserDatas;
using Dbconnection;
namespace T1d
{

    //--------------------------******main class*************----------------------//
    /*
     ***functions 
     *main()
            
     ***instance
     *uData             - instance of userData class under namespace userDatas
     *db                - instance of the dbConnection class under namespace Dbconnection
   
     ****variables
     *    chooseCase3, chooseCase4, chooseCase5 , choiceExistingUser, chooseCarb       - variables used to take a single string Y or N whether to rerun the loop
     * ch, choiceService                                 - variables used to take a single integer input to select an option

     */

    class MainProgram
     {

        static async Task Main()
        {
           
                Console.WriteLine("                             ***********       Welcome To Diabetes Management Solution  **************   ");
                Console.WriteLine("                          *************program to calulate blood sensitivity factor and carb count*******\n\n\n\n");


            try
            {
                
                UserData uData = new UserData();
                dbConnection db = new dbConnection();


                
                string chooseCase3 = "";
                string chooseCase4 = "";
                string chooseCase5 = "";
                string choiceExistingUser="";
                int choiceService=0;
                string chooseCarb="";
               

                Restart:
                        do{
                                try
                                {
                                        Console.Write("\n\tAre you a Registered User? Y or N\t:\t");
                                        choiceExistingUser = Console.ReadLine();
                                        if(choiceExistingUser != "y" && choiceExistingUser != "Y" && choiceExistingUser != "n" && choiceExistingUser != "N")
                                            {
                                                throw new Exception("\n\tcheck the input provided!! ");
                                            }
                                        break;
                                }
                                catch (FormatException e)
                                {
                                        Console.WriteLine("\n\t invalid input please try again");
                                }
                                catch(Exception e)
                                {
                                        Console.WriteLine(e.Message);
                                }
                                
                          } while (true);


                        if (choiceExistingUser.ToLower() == "n")
                        {
                            uData.userInfo();

                            int dbX = await uData.userInfoCheckDb(uData.fName, uData.lName);
                            Console.WriteLine(dbX);
                            if (dbX == 0)
                            {
                                await db.insertToDataBaseUserData(uData.fName, uData.lName, uData.age, uData.weight, uData.t, uData.dateofDiagnosis);
                            }
                            else
                            {

                                Console.WriteLine("\n\n\t\tYou are lready registered member Please provide existing customer");
                                goto Restart;
                            }
                           
                    
                    
                    uData.medication();
                    if (uData.typeOfMedication == 1)
                    {
                       do { 
                                    do
                                    {
                                        try
                                        {
                                            Console.WriteLine("\n\n\tAvailable services\n\t1.Insuin Sensitivity Factor\n\t2.Carb to insulin ratio\n\t3.Carb count\n\t4.Correction Factor-bolous\n\t5.Exit");
                                            Console.Write("\n\n\tSelect     -");
                                            choiceService = Convert.ToInt32(Console.ReadLine());
                                            if (choiceService > 5 || choiceService < 1)
                                            {
                                                throw new Exception("\n\tInvalid input enter a number between 1-5");
                                            }
                                            break;
                                        }
                                        catch (FormatException e)
                                        {
                                            Console.WriteLine("\n\t invalid input please try again");
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }

                                    } while (true);


                                        switch (choiceService)
                                        {
                                            case 1:
                                                uData.insuinSensitivityFactorC();
                                                break;
                                            case 2:
                                                uData.carbToInsulinRatioC();
                                                break;
                                            case 3:
                                                do
                                                {
                                                    Console.WriteLine("\n\n\t\t***** Carb Count section **********");
                                                    int ch;
                                                    do
                                                    {

                                                        try
                                                        {
                                                            Console.WriteLine("\n\t1.view item chart available\t2.insert your dish to list\t3.calculate carb to insulin\t4.Exit");
                                                            Console.Write("\n\n\tSelect     -");
                                                            ch = Convert.ToInt32(Console.ReadLine());
                                                            if (ch > 4 || ch < 1)
                                                            {
                                                                throw new Exception("\n\tInvalid input enter a number between 1-3");
                                                            }
                                                            break;
                                                        }
                                                        catch (FormatException e)
                                                        {
                                                            Console.WriteLine("\n\t invalid input please try again");
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            Console.WriteLine(e.Message);
                                                        }


                                                    } while (true);

                                                    switch (ch)
                                                    {
                                                        case 1:
                                                            await uData.fetchList();
                                                            break;
                                                        case 2:
                                                            await uData.insertItemToList();
                                                            break;
                                                        case 3:
                                                            double cc = await uData.carbCount();
                                                            Console.WriteLine("\n\n\tcarbcount details - insulin required : " + cc);
                                                            break;
                                                        case 4:
                                                            Environment.Exit(0);
                                                            break;
                                                        default:
                                                            Console.WriteLine("\n\tplease enter a valid choice");
                                                            break;
                                                    }
                                                    do
                                                    {
                                                        try
                                                        {
                                                            Console.Write("\n\n\tDo you want to continue in the carb count section?  Y or N:  ");
                                                            chooseCarb = Console.ReadLine();
                                                            if (chooseCarb != "y" && chooseCarb != "Y" && chooseCarb != "n" && chooseCarb != "N")
                                                                        {
                                                                throw new Exception("\n\tcheck the input provided!! ");
                                                            }
                                                            break;
                                                        }
                                                        catch (FormatException e)
                                                        {
                                                            Console.WriteLine("\n\t invalid input please try again");
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            Console.WriteLine(e.Message);
                                                        }

                                                    } while (true);

                                                } while (chooseCarb.ToLower() == "y");

                                                break;
                                            case 4:
                                                do
                                                {
                                                    Console.WriteLine("\n\n\t\t***** Correction factor section **********");
                                                    int ch;
                                                    do
                                                    {
                                                        try
                                                        {
                                                            Console.WriteLine("\n\t1.Correction Factor\t2.Correction Factor with Carb Count\t3.Exit");
                                                            Console.Write("\n\n\tSelect     -");
                                                            ch = Convert.ToInt32(Console.ReadLine());
                                                            if (ch > 3 || ch < 1)
                                                            {
                                                                throw new Exception("\n\tInvalid input enter a number between 1-3");
                                                            }
                                                            break;
                                                        }
                                                        catch (FormatException e)
                                                        {
                                                            Console.WriteLine("\n\t invalid input please try again");
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            Console.WriteLine(e.Message);
                                                        }

                                                    } while (true);

                                                    switch (ch)
                                                    {
                                                        case 1:
                                                            double cF = uData.correctionFactor();
                                                            Console.WriteLine("\n\n\tCorrection amount of Insulin required:  " + Math.Round(cF));
                                                            break;
                                                        case 2:
                                                            double ccAndCf = await uData.CFandCC();
                                                            Console.WriteLine("\n\n\tCorrection Factor with Carb count - Total insulin required\t:\t" + ccAndCf);
                                                            break;
                                                        case 3:
                                                            Environment.Exit(0);
                                                            break;
                                                        default:
                                                            Console.WriteLine("\n\tplease enter a valid choice");
                                                            break;
                                                    }
                                        do
                                        {
                                            try
                                            {
                                                Console.Write("\n\n\tDo you want to continue in the carb count section?  Y or N:  ");
                                                chooseCase4 = Console.ReadLine();
                                                if (chooseCase4 != "y" && chooseCase4 != "Y" && chooseCase4 != "n" && chooseCase4 != "N")
                                                {
                                                    throw new Exception("\n\tcheck the input provided!! ");
                                                }
                                                break;
                                            }
                                            catch (FormatException e)
                                            {
                                                Console.WriteLine("\n\t invalid input please try again");
                                            }
                                            catch (Exception e)
                                            {
                                                Console.WriteLine(e.Message);
                                            }

                                       } while (true);
                                     } while (chooseCase4.ToLower() == "y");

                                                break;

                                            case 5:
                                                Environment.Exit(0);
                                                break;
                                            default:
                                                Console.WriteLine("\n\tplease enter a valid choice");
                                                break;
                                        }
                        do
                        {
                                try
                                {
                                    Console.Write("\n\n\tcheck other services?  Y or N:  ");
                                    chooseCase5 = Console.ReadLine();
                                    if (chooseCase5 != "y" && chooseCase5 != "Y" && chooseCase5 != "n" && chooseCase5 != "N")
                                    {
                                        throw new Exception("\n\tcheck the input provided!! ");
                                    }
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\t invalid input please try again");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                            } while (true);
  

                    } while (chooseCase5.ToLower() == "y") ;
                }
            }
                  
                

            
                
                if (choiceExistingUser.ToLower() == "y")
                {

                    await uData.userInfoDb();
                    uData.medicationDb();
                    if (uData.typeOfMedicationDb == 1)
                    {
                        
                        int choice;
                        do
                        {
                                do
                                {
                                    try
                                    {
                                        Console.WriteLine("\n\n\tAvailable services\n\t1.Insuin Sensitivity Factor\n\t2.Carb to insulin ratio\n\t3.Carb count\n\t4.Correction Factor-bolous\n\t5.Exit");
                                        Console.Write("\n\n\tSelect     -");
                                        choice = Convert.ToInt32(Console.ReadLine());
                                        if(choice > 5 ||  choice < 1)
                                        {
                                            throw new Exception("\n\tInvalid input");
                                        }
                                        break;
                                    }
                                    catch (FormatException e)
                                    {
                                        Console.WriteLine("\n\t invalid input please try again");
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                               
                                }while (true);
                            

                                        switch (choice)
                                        {
                                            case 1:
                                                uData.insuinSensitivityFactorC();
                                                break;
                                            case 2:
                                                uData.carbToInsulinRatioCDb();
                                                break;
                                            case 3:
                                                        do
                                                        {
                                                            Console.WriteLine("\n\n\t\t***** Carb Count section **********");
                                                            int ch;
                                                            do
                                                            {

                                                                try
                                                                {
                                                                    Console.WriteLine("\n\t1.view item chart available\t2.insert your dish to list\t3.calculate carb to insulin\t4.Exit");
                                                                    Console.Write("\n\n\tSelect     -");
                                                                    ch = Convert.ToInt32(Console.ReadLine());
                                                                    if (ch > 4 || ch < 1)
                                                                    {
                                                                        throw new Exception("\n\tInvalid input enter a number between 1-3");
                                                                    }
                                                                    break;
                                                                }
                                                                catch (FormatException e)
                                                                {
                                                                    Console.WriteLine("\n\t invalid input please try again");
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    Console.WriteLine(e.Message);
                                                                }


                                                            } while (true);
                                                            switch (ch)
                                                            {
                                                                case 1:
                                                                    await uData.fetchList();
                                                                    break;
                                                                case 2:
                                                                    await uData.insertItemToList();
                                                                    break;
                                                                case 3:
                                                                    double cc = await uData.carbCountDb();
                                                                    Console.WriteLine("\n\n\tcarbcount details - insulin required : " + cc);
                                                                    break;
                                                                case 4:
                                                                    Environment.Exit(0);
                                                                    break;
                                                                default:
                                                                    Console.WriteLine("\n\tplease enter a valid choice");
                                                                    break;
                                                            }

                                                            do
                                                            {
                                                                try
                                                                {
                                                                    Console.Write("\n\n\tDo you want to continue in the carb count section?  Y or N:  ");
                                                                    chooseCase3 = Console.ReadLine();
                                                                    if (chooseCase3 != "y" && chooseCase3 != "Y" && chooseCase3 != "n" && chooseCase3 != "N")
                                                                    {
                                                                        throw new Exception("\n\tcheck the input provided!! ");
                                                                    }
                                                                    break;
                                                                }
                                                                catch (FormatException e)
                                                                {
                                                                    Console.WriteLine("\n\t invalid input please try again");
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    Console.WriteLine(e.Message);
                                                                }

                                                            } while (true);

                                                        } while (chooseCase3.ToLower() == "y");

                                                        break;
                                            case 4:
                                                        do
                                                        {
                                                                    Console.WriteLine("\n\n\t\t***** Correction factor section **********");
                                                                    int ch;
                                                                        do
                                                                        {
                                                                            try
                                                                            {
                                                                                Console.WriteLine("\n\t1.Correction Factor\t2.Correction Factor with Carb Count\t3.Exit");
                                                                                Console.Write("\n\n\tSelect     -");
                                                                                ch = Convert.ToInt32(Console.ReadLine());
                                                                                if (ch > 3 || ch < 1)
                                                                                {
                                                                                    throw new Exception("\n\tInvalid input enter a number between 1-3");
                                                                                }
                                                                                break;
                                                                            }
                                                                            catch (FormatException e)
                                                                            {
                                                                                Console.WriteLine("\n\t invalid input please try again");
                                                                            }
                                                                            catch (Exception e)
                                                                            {
                                                                                Console.WriteLine(e.Message);
                                                                            }

                                                                        } while (true);

                                                                    switch (ch)
                                                                    {
                                                                        case 1:
                                                                            double cF = uData.correctionFactor();
                                                                            Console.WriteLine("\n\n\tCorrection amount of Insulin required:  " + Math.Round(cF));
                                                                            break;
                                                                        case 2:
                                                                            double ccAndCf = await uData.CFandCC();
                                                                            Console.WriteLine("\n\n\tCorrection Factor with Carb count - Total insulin required\t:\t" + ccAndCf);
                                                                            break;
                                                                        case 3:
                                                                            Environment.Exit(0);
                                                                            break;
                                                                        default:
                                                                            Console.WriteLine("\n\tplease enter a valid choice");
                                                                            break;
                                                                    }

                                                                    do
                                                                    {
                                                                        try
                                                                        {
                                                                            Console.Write("\n\n\tcheck other services?  Y or N:  ");
                                                                            chooseCase4 = Console.ReadLine();
                                                                            if (chooseCase4 != "y" && chooseCase4 != "Y" && chooseCase4 != "n" && chooseCase4 != "N")
                                                                            {
                                                                                throw new Exception("\n\tcheck the input provided!! ");
                                                                            }
                                                                            break;
                                                                        }
                                                                        catch (FormatException e)
                                                                        {
                                                                            Console.WriteLine("\n\t invalid input please try again");
                                                                        }
                                                                        catch (Exception e)
                                                                        {
                                                                            Console.WriteLine(e.Message);
                                                                        }

                                                                    } while (true);

                                                        } while (chooseCase4.ToLower() == "y");

                                            break;

                                case 5:
                                    Environment.Exit(0);
                                    break;
                                default:
                                    Console.WriteLine("\n\tplease enter a valid choice");
                                    break;
                            }


                            do
                            {
                                try
                                {
                                    Console.Write("\n\n\tcheck other services?  Y or N:  ");
                                    chooseCase5 = Console.ReadLine();
                                    if (chooseCase5 != "y" && chooseCase5 != "Y" && chooseCase5 != "n" && chooseCase5 != "N")
                                    {
                                        throw new Exception("\n\tcheck the input provided!! ");
                                    }
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\t invalid input please try again");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                            } while (true);
                        } while (chooseCase5.ToLower() == "y");
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            finally
            {
                Console.Write("\n\n\tPress any Key to Exit  : ");
                Console.ReadKey();
            }
        }
    }

   //----------------------************end of main class********** ---------------//

}
