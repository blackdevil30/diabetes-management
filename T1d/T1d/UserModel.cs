﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace UserModelConfig
{
    /*---------UserModel class is used for storing data about the user, MongoDb is the database used
    
     *  id           : Bson id , unquie id generated for each documenct
     *  fName         : it is the first name of the user
     *  lName         : it is the last name of the user
     *  age           : age of the user
     *  weight        : weight of the user
     *  type          : type of diabetes the user is diagnosed
     *  dateOfDiagnosis: date on which the user is diagnosed with diabetes

    ----------- */
    public class UserModel
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string fName { get; set; }

        public string lName { get; set; }
        public int age { get; set; }

        public double weight { get; set; }

        public string type { get; set; }

        public DateTime dateOfDiagnosis { get; set; }


    }
}
