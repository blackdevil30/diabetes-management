﻿

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DataModel
{
    /*-------Data model class- its a model created for the data to stored in database - Mongodb is the database used
     * id           : Bson id , unquie id generated for each documenct
     * itemId       : An integer number provided for the items stored, trigger function availible in mongoDb is used for this
     *  item        : it is to provide the name of the item
     *  type        : its the type of the item
     *  subType     : sub type of the Item
     *  carbPer100  : carbohydrate content per 100 g of the item
     *  
     *  
     *  ***Reference for the below code
     *  MongoDb official doc
     * IAm Tim Corey videos

     */

    public class dataModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        public string Id { get; set; }

        public int itemId;
        public string item { get; set; }

        public string type { get; set; }

        public string subType { get; set; }
        
        public int carbPer100 { get; set; }
         
    }

   
}
