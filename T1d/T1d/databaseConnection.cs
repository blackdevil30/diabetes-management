﻿using System;
using DataModel;
using UserModelConfig;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;



namespace Dbconnection
{
    /*--------------Class for all database working the class contains database connection and quering
         * connectionString             -   variable used to store the mongodb atlas connection string
         * dName                        -   name of the database stored in this variable
         * carbFactorCollection         -   variable used for storing carbFactor collection in the database
         * 
         * //major functions inside the class are
         * ConnectToMongo()               - geneeric function used to connect to the database
         * insertToDataBaseFoodItems()    - function to insert an item to database
         * FetchAll()                     - Function to fetch all the items in the database
         * FetchDbItem()                  - Function to fetch a item from the database
         * 
       ***Reference for the below code
             *  MongoDb official doc
             * IAm Tim Corey videos
         * 
     --------------*/
    public class dbConnection
    {
        MongoClient dbClient = null;
        const string connectionString = "mongodb+srv://nj96official:12332166@cluster0.ivbcs.mongodb.net/foodItems?retryWrites=true&w=majority";
        private const string dName = "foodItems";
        private const string carbFactorCollection = "carbFactor";
        private const string userDataCollection = "userData";

        /*--------Generic function to connect to database
         * dbClient             - instance of mongoClient used
         ---------*/
        private IMongoCollection<T> ConnectToMongo<T>(in string collection)
        {
            try
            {
                dbClient = new MongoClient(connectionString);
            }
            catch(Exception e)
            {
                Console.WriteLine("failed to connect!!");
                Console.WriteLine(e.Message);
            }
            
            
                var db = dbClient.GetDatabase(dName);
                return db.GetCollection<T>(collection);

        }
        //-------end of ConnectToMongo()----------



        /*--------Function insertToDataBaseFoodItems()- to insert an item
         * item                 - parameter passed to function which contain item name
         * itemType             - parameter passed to function which contain item type
         * sub_type             - parameter passed to function which contain item sub type
         * carbFper100          - paramater passed to function which contains the value of carb Factor per 100 gram
         ----------*/

        public async Task insertToDataBaseFoodItems(string item, string itemType, string sub_Type, int carbFPer100)
        {
            try
            {

                var collection = ConnectToMongo<dataModel>(carbFactorCollection);

                var dModel = new dataModel { item = item, type = itemType, subType = sub_Type, carbPer100 = carbFPer100};

                await collection.InsertOneAsync(dModel);
               
            }
            catch (Exception e)
            {
               
                Console.WriteLine(e.Message);
            }


         }

        //--------end of insertToDataBaseFoodItems()---------



        /*---Functions to fetch all the items from database foodItems---*/

        public async Task<List<dataModel>> FetchAll()
        {

                var carbFactorcollections = ConnectToMongo<dataModel>(carbFactorCollection);

                var results = await carbFactorcollections.FindAsync(_ => true);

                return results.ToList();

        }
        //-----end of FetchAll()--------



        /*---Functions to fetch an item from database foodItems---*/
        public async Task<List<dataModel>> FetchDbItem(int itemNumber)
        {

            var carbFactorcollections = ConnectToMongo<dataModel>(carbFactorCollection);

            var results = await carbFactorcollections.FindAsync(c => c.itemId == itemNumber);

            return results.ToList();


        }
        //-------end of FetchDbItem()------

        /*Function to fetch User from userDb */
        public async Task<List<UserModel>> FetchDbUser(string fname, string lname) 
        {
            var studentCollections = ConnectToMongo<UserModel>(userDataCollection);
            var results = await studentCollections.FindAsync(c => c.fName == fname && c.lName == lname);
            return results.ToList();
        }

        //-----end of FetchDbUser(string fname, string lname


        /*--------Function insertToDataBaseUserData()- to insert an item
         
                      *fName            : parmeter passed to function which contains First name of the user
                      *lName            : parmeter passed to function which contains  Last name of the user
                      *age              : parmeter passed to function which contains age of the user
                      *bweight          : parmeter passed to function which contains the current weight of the user
                      *type             : parmeter passed to function which contains  the type of diabetic that the user or person who in touch with the user has it can type one, type 2 or Modi
                      *dat              : parmeter passed to function which contains the date of diagnosis
                     
                   
         ----------*/
        public async Task insertToDataBaseUserData(string fname ,string lname, int age, double bweight, string type, DateTime dat)
        {
            try
            {

                var collection = ConnectToMongo<UserModel>(userDataCollection);

                var uModel = new UserModel{fName= fname, lName = lname,age = age ,weight = bweight, type= type,dateOfDiagnosis= dat };

                await collection.InsertOneAsync(uModel);

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }


        }

        //--------end of insertToDataBaseUserData()---------
    }

}