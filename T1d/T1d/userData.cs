﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MajorCalculations;
using Dbconnection;
using System.Text.RegularExpressions;


namespace UserDatas
{
    //---------------------------------------**********class User data*********-----------------------------------------------------// 
   
    
    /*-------structure created for storing item dtails - used for storing item details retrived from database as list of structures
             * itemName         - name of the item is stored in this
             * carbount         - carb count of thr item per 100 gram
    -------*/
    struct Item
    {
        public string itemName;
        public int carbCount;
    }


    /*----------User data class is mainly used to gather user information
     * major functions inside- 
             * userInfo()
             * userInfoDb()
             * userInfoCheckDb()
             * medication() 
             * medicationDb()
             * insuinSensitivityFactorC()
             * carbToInsulinRatioC()
             * carbToInsulinRatioCDb()
             * correctionFactor()
             * CFandCC()
             * carbCount()
             * carbCountDb()
             * fetchList()
             * insertItemToList()

     * -----------*/
    public class UserData
    {
        

        calculations cal = new calculations();
        public int dailyTotalDose = 0;
        public int dailyTotalDoseDb = 0;
        public double weight = 0.0;
        public int typeOfMedication = 0;
        public int typeOfMedicationDb = 0;
        public string fName="";
        public string lName = "";
        public int age = 0;
        dbConnection db = new dbConnection();
        public string t ="";     //  t variable is used to store type of diabetic 
        
        public int type = 0;
      
        string fnam = "";
        string lnam = "";
        int a = 0;
        double weig = 0.0;
        string typ = "";
        public DateTime dateOf;
        public DateTime dateofDiagnosis = new DateTime(1900,01, 01);
        Regex x = new Regex(@"[yYnN]"); //
       // Regex d = new Regex(@"\d{2}-\w[a-zA-Z]{3}-\d{4}\s\d{2}:\d{2}:\d{2}\s[AM]");

        /*----------Function userInfo() -to gert the  basic user data
                    
                      *fName            : First name of the user
                      *lName            : Last name of the user
                      *age              : age of the user
                      *weight           : the current weight of the user
                      *type             : the type of diabetic that the user or person who in touch with the user has it can type one, type 2 or Modi
                      *dailyTotalDose   : total dose taken in a day
                      *t                :  is used to store the type of diabetic i string format the entire name
        ------------*/

        public void userInfo()
        {

            
            try
            {
                Regex alphabet = new Regex(@"[A-Za-z]");
                do
                {
                    Console.Write("\n\tEnter your First name:    ");
                    fName = Console.ReadLine();
                    
                } while (fName == "" || !alphabet.IsMatch(fName));

                do 
                {
                    Console.Write("\n\tEnter your Last name:    ");
                    lName = Console.ReadLine();
                } while (fName == "" || !alphabet.IsMatch(lName));

                do
                {
                    try
                    {
                        Console.Write("\n\tEnter your Age:  ");
                        age = Convert.ToInt32(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    
                } while (true);

                do
                {
                    try
                    {
                        Console.Write("\n\tEnter your weight in kg:  ");
                        weight = Convert.ToDouble(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                } while (true);



                do
                {
                    try
                    {
                        Console.WriteLine("\n\n\tChoose the type of diabetic you are:\n\n\t1.Type one diabetic\n\t2.Type two diabetic\n\t3. Modi Diabetic\n\n     ");
                        Console.Write("\n\tType   -");
                        type = Convert.ToInt32(Console.ReadLine());
                        if(type> 3 || type < 1)
                        {
                            throw new Exception("\n\tInvalid input");
                        }
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                } while (true);


                if (type == 1)
                {
                    t = "type one";

                }
                else if (type == 2)
                {
                    t = "type two";
                }
                else
                {
                    t = "modi";
                }
                do {

                        try
                        {
                            Console.Write("\n\n\tEnter the date of diagnosis yyyy/mm/dd:     ");

                            dateofDiagnosis = DateTime.Parse(Console.ReadLine());
                            
                        
                            break;

                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("\n\n\tinvalid input");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    } while (true);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //------end of userInfo()--------


        /*---------Function userInfoDb() is used to get the single user data and store it in diiferent variable
        
         * firstName            - local variable used to store first name of the user
         * lastName             - local variable used to store last name of the user
         * fnam                 - global variable used for storing first name retrived from database
         * lam                  - global variable used for storing last name retrived from database
         * a                    - global variable used for storing age retrived from database
         * weig                 - global variable used for storing weight retrived from database
         * typ                  -global variable used for storing type of diabetes retrived from database
         * dateOf               - global variable used for storing date of diagnosis retrived from database 
        ----------- */
        public async Task userInfoDb()
        {
            Regex alpha = new Regex(@"[A-za-z]");
            string firstName;
            string lastName;
            try
            {
              
                do
                {
                    Console.Write("\n\n\tEnter the first name :\t");
                    firstName = Console.ReadLine();
                } while (!alpha.IsMatch(firstName));

                do
                {
                    Console.Write("\n\tEnter the last name :\t");
                    lastName = Console.ReadLine();
                } while (!alpha.IsMatch(lastName));


                var singleUser = await db.FetchDbUser(firstName, lastName);

                foreach (var s in singleUser.ToList())
                {
                    fnam = s.fName;
                    lnam = s.lName;
                    a = s.age;
                    weig = s.weight;
                    typ = s.type;
                    dateOf = s.dateOfDiagnosis;
                }
            }
           
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        //-------end ofuserInfoDb()-------


        /***-----  Function userInfoCheckDb() used to check a single used exist
         *fn                   - parameter passed with the first name
         *ln                   -parameter passed with the last name
         *firstName            - local variable used to store fn
         *lastName             - local variable used to store last name
         * a                   - vaiable with integer value 0 and 1 , 0 for no such user and 1 for existing user
         * ------******/
        public async Task<int> userInfoCheckDb(string fn, string ln)
        {
            int a = 0;
                string firstName = fn;
                string lastName = ln;
                try
                {
                    var singleUser = await db.FetchDbUser(firstName, lastName);
                    foreach (var s in singleUser.ToList())
                    {

                        if ((s.fName == "") && (s.lName == "") && (s.age == 0) && (s.weight == 0) && (s.type == ""))
                        {
                            a = 0;

                        }
                        else
                        {
                            a = 1;
                        }
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
               
            return a;

        }


        //----------end of userInfoCheckDb()-------------------





        /*------------function medication() - contails all medication details
         *type             : the type of diabetic that the user or person who in touch with the user has it can type one, type 2 or Modi
         *count             : variable set to cunt the number of time user is attempting , if user attempts wrong entry more than three it exits
         *ch                : vaiable stores Y or N which indicates yes or no
         ------------*/

        public void medication() 
        {
            try
            {
                
                int count = 0;  //count varible is used to store the number of time the do loop is repeated an user is given three chance
                if (type == 1 || type == 2 || type == 3)

                {
   
                    string ch ="";
                    do
                    {
                        do 
                        {
                            try
                            {
                                Console.WriteLine("\n\n\tEnter the type of medication\n\t1.Insulin\n\t2.No Insulin");
                                Console.Write("\n\n\tType   -");
                                typeOfMedication = Convert.ToInt32(Console.ReadLine());
                                if (typeOfMedication > 2 || typeOfMedication < 1)
                                {
                                    throw new Exception("Invalid input");
                                }
                                break;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("\n\n\tinvalid input");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }

                        } while (true);
                        
                        
                        count = count + 1;
                        
                        if (typeOfMedication == 1)
                        {

                            dailyTotalDose = cal.totalDoseCalculation();

                        }
                        else
                        {
                            Console.WriteLine("\n\t\tsorry there is no available service for you!!!");
                            do
                            {

                                Console.Write("\n\tDo you want to go back to previous option? Y or N\t-\t");
                                ch = Console.ReadLine();
                            } while (!x.IsMatch(ch));
                           
                            if (ch.ToLower() == "y" && count > 3)
                            {
                                Console.WriteLine("\n\n\n\t\t\tyou have tried many times");
                                break;
                              
                            }
                            if (ch.ToLower() == "n")
                            {
                                Environment.Exit(0);    // Refered from c# official docs
                            }

                        }
                    } while (ch.ToLower() == "y");
                }
            
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            

        }

        //-----End of medication()------


        /*------------function medicationDb() - contails all medication details which are retrieved from database
         * typ              - variable used for storing type of diabetes
         * count            - variable set to cunt the number of time user is attempting , if user attempts wrong entry more than three it exits
         *ch                -  vaiable stores Y or N which indicates yes or no
       
        ------------*/

        public void medicationDb()
        {
            try
            {
              
                int count = 0;  //count varible is used to store the number of time the do loop is repeated an user is given three chance
                if (typ == "type one" || typ == "type two" || typ == "modi")

                {

                    string ch = "";
                    do
                    {

                        do
                        {
                            try
                            {
                                Console.WriteLine("\n\n\tEnter the type of medication\n\t1.Insulin\n\t2.No Insulin");
                                Console.Write("\n\n\tType   -");
                                typeOfMedicationDb = Convert.ToInt32(Console.ReadLine());
                                if (typeOfMedicationDb > 2 || typeOfMedicationDb < 1)
                                {
                                    throw new Exception("Invalid input");
                                }
                                break;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("\n\n\tinvalid input");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }

                        } while (true);


                        count = count + 1;

                        if (typeOfMedicationDb == 1)
                        {

                            dailyTotalDose = cal.totalDoseCalculation();

                        }
                        else
                        {
                            Console.WriteLine("\n\t\tsorry there is no available service for you!!!");

                            do
                            {

                                Console.Write("\n\tDo you want to go back to previous option? Y or N\t-\t");
                                ch = Console.ReadLine();
                            } while (!x.IsMatch(ch));

                            if (ch.ToLower() == "y" && count > 3)
                            {
                                Console.WriteLine("\n\n\n\t\t\tyou have tried many times");
                                break;

                            }
                            if (ch.ToLower() == "n")
                            {
                                Environment.Exit(0);              // Refered from c# official docs
                            }

                        }
                    } while (ch.ToLower() == "y");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


        }

        //------end of medicationDb()------


        /*--------- Function insuinSensitivityFactorC() -to calcuate insulin sensitivity factor
          insulinSensityvityFactor : variable used to store insulin sensitivity factor, 
          Insulin sensitivity factor(ISF)-it is to identifify blood sensitivity towards insulin.
          ------------*/
        public void insuinSensitivityFactorC()
        {
          
            try
            {
                int insulinSensitivityFactor = cal.isf(dailyTotalDose);
                Console.WriteLine("\n\n\tYour Insulin Sensitivity Factor:\t" + insulinSensitivityFactor);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
        }
        //-------------

        /*------- Function carbToInsulinRatioC() -to calculate carb to insulin ratio,  
                 * Carb To Insulin Ratio(CIR) - it gives the detail about the insuin required for 1 gram of carb
                 * carbToInsulinRatio : Variable used to store CIR  
        ----*/
        public void carbToInsulinRatioC()
        {

            try
            {
 
                double carbToInsulinRatio = cal.cir(dailyTotalDose, weight);

                Console.WriteLine("\n\n\tYour Carb to Insulin Ratio:\t" + carbToInsulinRatio);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        //----------



        /*------- Function carbToInsulinRatioCDb() -to calculate carb to insulin ratio,  
                 * Carb To Insulin Ratio(CIR) - it gives the detail about the insuin required for 1 gram of carb
                 * carbToInsulinRatio : Variable used to store CIR  
                 * w                  : variable used to store current weight of the person 
        ----*/
        public void carbToInsulinRatioCDb()
        {

            try
            {
                double w;
                do
                {
                    try
                    {
                        Console.Write("\n\tEnter your weight in kg:  ");
                         w = Convert.ToDouble(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                } while (true);



                double carbToInsulinRatio = cal.cir(dailyTotalDose, w);

                Console.WriteLine("\n\n\tYour Carb to Insulin Ratio:\t" + carbToInsulinRatio);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
 
        }
        //----end of carbToInsulinRatioCDb()------





        /*-----------------Function correctionFactor() -to calculate correction factor 
         * currentSugarlevel ,csl           - variabe used to store current sugar level reading
         * requiredLevel ,rsl               - variable used to store required sugar level to be met
         --------*/


        public double correctionFactor()
        {
            double currentSugarlevel = 0, requiredLevel = 0, csl = 0, rsl = 0, correctionFactor =0;


            try
            {
                int choice;
                do
                {
                    try 
                    {
                        Console.WriteLine("\n\tEnter the unit you use for measuring blood glucose choose one :\n\t1.mmol/l  - (sample: 6.5)\n\t2.mg/dl  - (sample - 120)");
                        Console.Write("\n\n\tChoice -\t");
                        choice = Convert.ToInt32(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (true);

                if (choice == 1)
                {
                    do
                    {
                        try
                        {
                            Console.Write("\n\n\tPlease provide the current sugar level reading  in mmol/l   :   ");
                            csl = Convert.ToDouble(Console.ReadLine());
                            currentSugarlevel = Math.Round(cal.convertSugarLevel(csl));
                            break;
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("\n\n\tinvalid input");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    } while (true);

                    do
                    {
                        try
                        {
                            Console.Write("\n\n\tplease provide the required sugar level reading  in mmol/l  :   ");
                            rsl = Convert.ToDouble(Console.ReadLine());
                            requiredLevel = Math.Round(cal.convertSugarLevel(rsl));
                            break;
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("\n\n\tinvalid input");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    } while (true);

                   
                }

                if (choice == 2)
                {

                    do
                    {
                        try
                        {
                            Console.Write("\n\n\tPlease provide the current sugar level reading  in mg/dl   :   ");
                            currentSugarlevel = Convert.ToInt32(Console.ReadLine());
                            break;
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("\n\n\tinvalid input");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    } while (true);

                    do
                    {
                        try
                        {
                            Console.Write("\n\n\tplease provide the required sugar level reading  in mg/dl  :   ");
                            requiredLevel = Convert.ToInt32(Console.ReadLine());
                            break;
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("\n\n\tinvalid input");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    } while (true);
                }


                int insulinSensitivityFactor = cal.isf(dailyTotalDose);
                 correctionFactor = cal.correctionFactorFunc(currentSugarlevel, requiredLevel, insulinSensitivityFactor);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            return correctionFactor;
            
           

        }


        /*-------
            Function CFandCC() -to calculate both carb count and correction factor
            *cc                 - is used to store carb count
            *cf                 - is used to store correction factor
            *tottalFactor       - is used to store the total of cc and cf , is the total amount of insulin to take
            
         ------*/
        public async Task<double> CFandCC()
        {
            double totalFactor = 0;
            try
            {
                Console.WriteLine("\n\t\t-------Carb Count----- ");
                double cc = await carbCount();
                Console.WriteLine("\n\t\t-------Correction Factor----- ");
                double cf = correctionFactor();
                totalFactor = cc + cf;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            return totalFactor;
           
        }

        //--------------


        /*---------Function  carbCount() to calculate the carb
         * carbFM                   - the variable is to store carbohydrate factor per 100 gram
         * totalcarbF               - the varaible is used to store sum of total carbohydrate factor obtained from carbFM
         * qIntake                  - variable used to store total quantity intake
         * carbohydrateCount        - variable stores the final result of the carbohydrate count
        
         * carbFDb                  - the variable is to store carbohydrate factor per 100 gram which is taken from the database
         * qIntakeDb                - variable used to store total quantity intake in the non manual section
         *  totalcarbFDb            -  the varaible is used to store sum of total carbohydrate factor obtained from carbFDb
         
         ----------*/
        public async Task<double> carbCount()
        {

            double x = 0.0, carbFM, totalcarbF = 0.0, qIntake = 0.0, carbohydrateCount = 0;
            try
            {
               
                double carbToInsulinRatio = cal.cir(dailyTotalDose, weight);

                int ch;
                do
                {
                    try
                    {
                        Console.Write("\n\tDo you want to calculate\t1.Manually\t2.select from item list Avialible  :\t");
                        ch = Convert.ToInt32(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (true);


                
                if (ch == 1)
                {
                    try
                    {
                        int numOfItem;
                        do
                        {
                            try
                            {
                                Console.Write("\n\n\tEnter the number of items you are going to have\t:\t");
                                numOfItem = Convert.ToInt32(Console.ReadLine());
                                break;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("\n\n\tinvalid input");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        } while (true);


                        for (int i = 1; i <= numOfItem; i++)
                        {
                            do
                            {
                                try
                                {
                                    Console.Write("\n\tEnter the number of carb per 100 gram in the\t" + i + "\tItem   :   ");
                                    carbFM = Convert.ToInt32(Console.ReadLine());
                                    Console.Write("\n\tEnter the number of gram -intake of\t" + i + "\tItem :   ");
                                    qIntake = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\n\tinvalid input");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (true);
                            do
                            {
                                try
                                {
                                    Console.Write("\n\tEnter the number of gram -intake of\t" + i + "\tItem :   ");
                                    qIntake = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\n\tinvalid input");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (true);

                            x = cal.calCarb(carbFM, qIntake);

                            totalcarbF += x;

                        }

                        carbohydrateCount = cal.carbCount(carbToInsulinRatio, totalcarbF);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }

                if (ch == 2)
                {
                    try
                    {
                        Console.WriteLine("\n\n\t\t\t-----************Available List of Items*********************----------");
                        await fetchList();
                        int numOfItem;


                        do
                        {
                            try
                            {
                                Console.Write("\n\n\n\tEnter the number of items you are going to have\t:\t");
                                numOfItem = Convert.ToInt32(Console.ReadLine());
                                break;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("\n\n\tinvalid input");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        } while (true);

                        
                        string keyV = "";
                        int val = 0;
                        double carbFDb = 0.0, qIntakeDb = 0.0, xDb = 0, totalcarbFDb = 0.0;


                        Item[] itemCarb = new Item[numOfItem];
                        for (int i = 0; i < numOfItem; i++)
                        {
                            int itemNumber;
                            do
                            {
                               
                                try
                                {
                                    Console.Write("\n\tEnter the item Number\t -\t");
                                    itemNumber = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\n\tinvalid input");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (true);


                            
                            var singleItem = await db.FetchDbItem(itemNumber);

                            foreach (var k in singleItem.ToList())
                            {
                                keyV = k.item;
                                val = k.carbPer100;
                            }
                            itemCarb[i].itemName = keyV;
                            itemCarb[i].carbCount = val;

                        }

                        for (int i = 0; i < itemCarb.Length; i++)
                        {
                            carbFDb = itemCarb[i].carbCount;
                            Console.Write("\n\tEnter the number of gram of \t" + itemCarb[i].itemName + "\tintake  :\t");
                            qIntakeDb = Convert.ToDouble(Console.ReadLine());
                            xDb = cal.calCarb(carbFDb, qIntakeDb);

                            totalcarbFDb += xDb;

                        }
                        carbohydrateCount = cal.carbCount(carbToInsulinRatio, totalcarbFDb);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return carbohydrateCount;
        }

        /*---------Function  carbCountDb() to calculate the carb
         * carbFM                   - the variable is to store carbohydrate factor per 100 gram
         * totalcarbF               - the varaible is used to store sum of total carbohydrate factor obtained from carbFM
         * qIntake                  - variable used to store total quantity intake
         * carbohydrateCount        - variable stores the final result of the carbohydrate count
        
         * carbFDb                  - the variable is to store carbohydrate factor per 100 gram which is taken from the database
         * qIntakeDb                - variable used to store total quantity intake in the non manual section
         *  totalcarbFDb            -  the varaible is used to store sum of total carbohydrate factor obtained from carbFDb
         *  w                       - variable used to store weight of the user
         
         ----------*/

        public async Task<double> carbCountDb()
        {
            
            double x = 0.0, carbFM, totalcarbF = 0.0, qIntake = 0.0,carbohydrateCount = 0;
            try
            {
                double w;
                do
                {
                    try
                    {
                        Console.Write("\n\tEnter your weight in kg:  ");
                        w = Convert.ToDouble(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                } while (true);

                double carbToInsulinRatio = cal.cir(dailyTotalDose, w);

                int ch;
                do
                {
                    try
                    {
                        Console.Write("\n\tDo you want to calculate\t1.Manually\t2.select from item list Avialible  :\t");
                        ch = Convert.ToInt32(Console.ReadLine());
                        if(ch> 2 || ch < 1)
                        {
                            throw new Exception("invalid input");
                        }
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                } while (true);



               
                if (ch == 1)
                {
                    int numOfItem;
                    try
                    {
                        do
                        {
                            try
                            {
                                Console.Write("\n\n\tEnter the number of items you are going to have\t:\t");
                                 numOfItem = Convert.ToInt32(Console.ReadLine());
                                break;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("\n\n\tinvalid input");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        } while (true);


                        for (int i = 1; i <= numOfItem; i++)
                        {
                            do
                            {
                                try
                                {
                                    Console.Write("\n\tEnter the number of carb per 100 gram in the\t" + i + "\tItem   :   ");
                                    carbFM = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\n\tinvalid input");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (true);

                            do
                            {
                                try
                                {
                                    Console.Write("\n\tEnter the number of gram -intake of\t" + i + "\tItem :   ");
                                    qIntake = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\n\tinvalid input");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (true);



                           
                            x = cal.calCarb(carbFM, qIntake);

                            totalcarbF += x;

                        }

                        carbohydrateCount = cal.carbCount(carbToInsulinRatio, totalcarbF);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    
                }

                if (ch == 2)
                {
                    int numOfItem;
                    try
                    {
                        Console.WriteLine("\n\n\t\t\t-----************Available List of Items*********************----------");
                        await fetchList();

                        do
                        {
                            try
                            {
                                Console.Write("\n\n\n\tEnter the number of items you are going to have\t:\t");
                                 numOfItem = Convert.ToInt32(Console.ReadLine());
                                break;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("\n\n\tinvalid input");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        } while (true);

                        
                        string keyV = "";
                        int val = 0;
                        double carbFDb = 0.0, qIntakeDb = 0.0, xDb = 0, totalcarbFDb = 0.0;


                        Item[] itemCarb = new Item[numOfItem];
                        for (int i = 0; i < numOfItem; i++)
                        {
                            int itemNumber;
                            do
                            {
                                try
                                {
                                    Console.Write("\n\tEnter the item Number\t -\t");
                                    itemNumber = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException e)
                                {
                                    Console.WriteLine("\n\n\tinvalid input");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (true);

                           
                            var singleItem = await db.FetchDbItem(itemNumber);

                            foreach (var k in singleItem.ToList())
                            {
                                keyV = k.item;
                                val = k.carbPer100;
                            }
                            itemCarb[i].itemName = keyV;
                            itemCarb[i].carbCount = val;

                        }

                        for (int i = 0; i < itemCarb.Length; i++)
                        {
                            carbFDb = itemCarb[i].carbCount;
                            Console.Write("\n\tEnter the number of gram of \t" + itemCarb[i].itemName + "\tintake  :\t");
                            qIntakeDb = Convert.ToDouble(Console.ReadLine());
                            xDb = cal.calCarb(carbFDb, qIntakeDb);

                            totalcarbFDb += xDb;

                        }
                        carbohydrateCount = cal.carbCount(carbToInsulinRatio, totalcarbFDb);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return carbohydrateCount;
        }
        //--------------end of carbCountDb()---------------------

        
        
        /*---------------Function fetchList() to display all the items in the table in the specified format -------***********/
        public async Task fetchList()
        {
            try
            {
                var data = await db.FetchAll();
                Console.WriteLine("\n\n\t\tItemNo\t\t\t\tItem\t\t\t\tcarb per 100g");

                foreach (var d in data.ToList())
                {
                    Console.WriteLine(value: $"\n\t\t{d.itemId}\t\t\t\t{d.item}\t\t\t\t{d.carbPer100}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        //-------end of  fetchList()-----



        /*----Function to insert item to foodItems Database
             * item                     -   varaible used to store the item name
             * itemType                 -   item type depending on the food item [example: continental,indian]
             * sub_type                 -   if there is any sub type for the food item
             * carbFPer100              -   carbohydrate content per 100g is stored in this variable

         -----*/
        public async Task insertItemToList()
        {
            string item="";
            string itemType = "";
            string sub_Type = "";
            int carbFPer100 = 0;
            try
            {
                Console.Write("\n\tEnter the Item   :   ");
                item = Console.ReadLine();
                Console.Write("\n\tEnter the item type  :   ");
                itemType = Console.ReadLine();
                Console.Write("\n\tEnter the sub type if any    :   ");
                sub_Type = Console.ReadLine();

                do
                {
                    try
                    {
                        Console.Write("\n\tEnter the carbohydrate content per 100g  :   ");
                        carbFPer100 = Convert.ToInt32(Console.ReadLine());
                        break;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("\n\n\tinvalid input");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (true);


               
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            

            await db.insertToDataBaseFoodItems(item, itemType, sub_Type, carbFPer100);
        }
        //----end of insertItemToList()-------


    }
    //------------------------************end of user class***********---------------------------//
}





   
